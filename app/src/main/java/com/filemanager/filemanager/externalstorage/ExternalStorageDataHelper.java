package com.filemanager.filemanager.externalstorage;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.widget.Toast;

import com.filemanager.filemanager.model.FileModel;
import com.filemanager.filemanager.ui.main.CreateNewFileListener;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ExternalStorageDataHelper {
    private static final Comparator<? super File> COMPARATOR = new Comparator<File>() {
        @Override
        public int compare(File o1, File o2) {
            return o1.getName().toUpperCase().compareTo(o2.getName().toUpperCase());
        }
    };
    private static File currentPath;

    public static final int FOLDER = 1;
    public static final int FILE = 2;

    public static List<FileModel> getData(File dataDir, boolean updateCurrentPatch) {
        if (updateCurrentPatch) {
            currentPath = dataDir;
        }
        List<FileModel> data = new ArrayList<>();

        if (dataDir.exists()) {
            File[] folders = dataDir.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return (file.isDirectory() && file.canRead());
                }
            });
            File[] files = dataDir.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    if (!file.isDirectory()) {
                        return file.canRead();
                    } else {
                        return false;
                    }
                }
            });

            if (folders != null && folders.length > 0) {
                sort(folders);
                for (File folder : folders) {
                    FileModel model = new FileModel();
                    model.setTitle(folder.getName());
                    model.setDirectory(true);
                    model.setLastUpdateData(getLastUpdateData(folder));
                    model.setFileSize(0);
                    model.setChildFileCount(folder.list().length);
                    data.add(model);
                }
            }

            if (files != null && files.length > 0) {
                sort(files);
                for (File file : files) {
                    FileModel model = new FileModel();
                    model.setTitle(file.getName());
                    model.setDirectory(false);
                    model.setLastUpdateData(getLastUpdateData(file));
                    model.setFileSize(file.length() / 1024);
                    model.setChildFileCount(0);
                    data.add(model);
                }
            }
        }
        return data;
    }

    private static void sort(File[] folders) {
        Arrays.sort(folders, COMPARATOR);
    }

    private static String getLastUpdateData(File folder) {
        File file = new File(currentPath, folder.getName());
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("LLL dd, yyyy");
        return sdf.format(file.lastModified());
    }

    public static List<FileModel> onClickItem(String folderName) {
        return getData(new File(currentPath, folderName), true);
    }

    public static boolean onBackPress() {
        return currentPath.equals(Environment.getExternalStorageDirectory());
    }

    public static List<FileModel> goToLastPage() {
        return getData(new File(currentPath.getParent()), true);
    }

    public static void openFile(Context context, FileModel model) {
        try {
            boolean notProgramToOpenFile = false;
            Uri uri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                uri = FileProvider.getUriForFile(context, "com.mydomain.fileprovider", new File(currentPath.getAbsolutePath() + "/" + model.getTitle()));
            } else {
                File file = new File(currentPath.getAbsolutePath() + "/" + model.getTitle());
                uri = Uri.fromFile(file);
            }

            String url = model.getTitle();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (url.contains(".doc") || url.contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.contains(".ppt") || url.contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.contains(".xls") || url.contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.contains(".zip")) {
                // ZIP file
                intent.setDataAndType(uri, "application/zip");
            } else if (url.contains(".rar")) {
                // RAR file
                intent.setDataAndType(uri, "application/x-rar-compressed");
            } else if (url.contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.contains(".wav") || url.contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.contains(".jpg") || url.contains(".jpeg") || url.contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.contains(".3gp") || url.contains(".mpg") ||
                    url.contains(".mpeg") || url.contains(".mpe") || url.contains(".mp4") || url.contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                Toast.makeText(context, "Can’t open file", Toast.LENGTH_SHORT).show();
                notProgramToOpenFile = true;
            }

            if (!notProgramToOpenFile) {
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                context.startActivity(intent);
            }
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No application found which can open the file", Toast.LENGTH_SHORT).show();
        }
    }

    public static List<FileModel> deleteItem(FileModel fileModel) {
        File file = new File(currentPath, fileModel.getTitle());
        deleteRecursive(file);
        return getData(currentPath, false);
    }

    private static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles()) {
                deleteRecursive(child);
            }
        fileOrDirectory.delete();
    }

    public static void addFile(String fileName, int fileType, CreateNewFileListener listener) {
        if (fileType == FOLDER) {
            File f = new File(currentPath, fileName);
            if (!f.exists()) {
                f.mkdirs();
                listener.onCreated(true);
            } else {
                listener.onCreated(false);
            }
        } else {
            try {
                File myFile = new File(currentPath, fileName);
                myFile.createNewFile();
                FileOutputStream fOut = new FileOutputStream(myFile);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                myOutWriter.close();
                fOut.close();
                listener.onCreated(true);
            } catch (Exception e) {
                listener.onCreated(false);
            }
        }
    }

    public static List<FileModel> updateList() {
        return getData(currentPath, false);
    }
}
