package com.filemanager.filemanager.model;

public class FileModel {

    private String title;
    private String lastUpdateData;
    private long childFileCount;
    private long fileSize;
    private boolean directory;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLastUpdateData() {
        return lastUpdateData;
    }

    public void setLastUpdateData(String lastUpdateData) {
        this.lastUpdateData = lastUpdateData;
    }

    public long getChildFileCount() {
        return childFileCount;
    }

    public void setChildFileCount(long childFileCount) {
        this.childFileCount = childFileCount;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public boolean isDirectory() {
        return directory;
    }

    public void setDirectory(boolean directory) {
        this.directory = directory;
    }
}
