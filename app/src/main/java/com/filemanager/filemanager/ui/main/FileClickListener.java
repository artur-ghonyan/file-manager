package com.filemanager.filemanager.ui.main;

import com.filemanager.filemanager.model.FileModel;

interface FileClickListener {
    void onClick(FileModel model);
    void onLongClick(FileModel model);
}
