package com.filemanager.filemanager.ui.main;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.filemanager.filemanager.R;
import com.filemanager.filemanager.externalstorage.ExternalStorageDataHelper;
import com.filemanager.filemanager.model.FileModel;
import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements FileClickListener, CreateNewFileListener {

    private FileManagerAdapter mAdapter;
    private List<FileModel> mData;
    private RecyclerView mRecyclerView;
    private Menu mMenu;
    private boolean mGridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.recycler_view);
        FloatingActionButton fat = findViewById(R.id.fab_add);

        if (isReadStoragePermissionGranted()) {
            mData = ExternalStorageDataHelper.getData(Environment.getExternalStorageDirectory(), true);
        } else {
            if (mData == null) {
                mData = new ArrayList<>();
            }
        }

        setLayoutManager();
        mAdapter = new FileManagerAdapter(this, mData, this, mGridLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        fat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickFloatingActionButton();
            }
        });
    }

    private void setLayoutManager() {
        if (mGridLayoutManager) {
            mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    private boolean isReadStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        } else {
            //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    private boolean isWriteStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        } else {
            //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mAdapter.setData(ExternalStorageDataHelper.getData(Environment.getExternalStorageDirectory(), true));
        } else {
            Toast.makeText(this, "permission is not granted", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.linear_layout:
                setMenuItemsVisibility(false);
                break;
            case R.id.grid_layout:
                setMenuItemsVisibility(true);
                break;
            default:
                break;
        }
        return true;
    }

    private void setMenuItemsVisibility(boolean gridLayoutVisible) {
        mMenu.findItem(R.id.linear_layout).setVisible(gridLayoutVisible);
        mMenu.findItem(R.id.grid_layout).setVisible(!gridLayoutVisible);

        mGridLayoutManager = gridLayoutVisible;
        setLayoutManager();
        mAdapter.setLayout(mGridLayoutManager);
    }

    @Override
    public void onClick(final FileModel model) {
        if (model.isDirectory()) {
            if (model.getChildFileCount() > 0) {
                mAdapter.setData(ExternalStorageDataHelper.onClickItem(model.getTitle()));
            } else {
                Toast.makeText(this, "No Items", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (isWriteStoragePermissionGranted()) {
                ExternalStorageDataHelper.openFile(this, model);
            }
        }
    }

    @Override
    public void onLongClick(final FileModel model) {
        if (isWriteStoragePermissionGranted()) {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Are you sure you want to delete?");
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mAdapter.setData(ExternalStorageDataHelper.deleteItem(model));
                            dialog.dismiss();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    }

    private void clickFloatingActionButton() {
        AddFileDialog dialog = new AddFileDialog(this, this);
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (ExternalStorageDataHelper.onBackPress()) {
            super.onBackPressed();
        } else {
            mAdapter.setData(ExternalStorageDataHelper.goToLastPage());
        }
    }

    @Override
    public void onCreated(boolean created) {
        if(created) {
            mAdapter.setData(ExternalStorageDataHelper.updateList());
        } else {
            Toast.makeText(this, "Could not create the file", Toast.LENGTH_SHORT).show();
        }
    }
}
