package com.filemanager.filemanager.ui.main;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.filemanager.filemanager.R;
import com.filemanager.filemanager.externalstorage.ExternalStorageDataHelper;

class AddFileDialog extends Dialog {
    private RadioGroup mRadioGroup;
    private int mFileType;

    private final CreateNewFileListener mListener;

    AddFileDialog(Context context, CreateNewFileListener listener) {
        super(context);
        mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_item);
        init();
    }

    private void init() {
        final EditText enterName = findViewById(R.id.action_enter_name);

        mFileType = ExternalStorageDataHelper.FOLDER;
        mRadioGroup = findViewById(R.id.radio_group);
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = mRadioGroup.getCheckedRadioButtonId();
                switch (selectedId) {
                    case R.id.rb_file:
                        mFileType = ExternalStorageDataHelper.FILE;
                        break;
                    case R.id.rb_folder:
                        mFileType = ExternalStorageDataHelper.FOLDER;
                        break;
                }
            }
        });

        findViewById(R.id.action_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fileName = enterName.getText().toString().trim();
                if (TextUtils.isEmpty(fileName)) {
                    enterName.setError("Name cannot be empty!");
                } else {
                    ExternalStorageDataHelper.addFile(fileName, mFileType, mListener);
                    dismiss();
                }
            }
        });

        findViewById(R.id.action_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}