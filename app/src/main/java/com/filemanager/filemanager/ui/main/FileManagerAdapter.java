package com.filemanager.filemanager.ui.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.filemanager.filemanager.R;
import com.filemanager.filemanager.model.FileModel;

import java.util.List;

public class FileManagerAdapter extends RecyclerView.Adapter<FileManagerAdapter.FileManagerHolder> {
    private final LayoutInflater mInflater;
    private final FileClickListener mListener;
    private List<FileModel> mData;
    private boolean mGridLayoutManager;

    FileManagerAdapter(Context context, List<FileModel> data, FileClickListener listener, boolean gridLayoutManager) {
        mData = data;
        mListener = listener;
        mGridLayoutManager = gridLayoutManager;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public FileManagerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        FileManagerHolder fileManagerHolder;
        if (position == 0) {
            fileManagerHolder = new FileManagerHolderGrid(mInflater.inflate(R.layout.file_item_grid, viewGroup, false));
        } else {
            fileManagerHolder = new FileManagerHolderList(mInflater.inflate(R.layout.file_item_list, viewGroup, false));
        }
        return fileManagerHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FileManagerHolder fileManagerHolder, int position) {
        fileManagerHolder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mGridLayoutManager) {
            return 0;
        } else {
            return 1;
        }
    }

    void setLayout(boolean gridLayoutManager) {
        mGridLayoutManager = gridLayoutManager;
        notifyDataSetChanged();
    }

    void setData(List<FileModel> data) {
        mData = data;
        notifyDataSetChanged();
    }

    abstract class FileManagerHolder extends RecyclerView.ViewHolder {

        FileManagerHolder(@NonNull View itemView) {
            super(itemView);
        }

        abstract void onBind(final int position);
    }

    class FileManagerHolderList extends FileManagerHolder {
        private final ImageView labelImageView;
        private final TextView labelTitle;
        private final TextView labelLastUpdateData;
        private final TextView labelChildFileCount;
        private final TextView labelFileSize;

        FileManagerHolderList(@NonNull View itemView) {
            super(itemView);
            labelImageView = itemView.findViewById(R.id.label_image_view);
            labelTitle = itemView.findViewById(R.id.label_view_title);
            labelLastUpdateData = itemView.findViewById(R.id.label_last_update_data);
            labelChildFileCount = itemView.findViewById(R.id.label_file_count);
            labelFileSize = itemView.findViewById(R.id.label_file_size);
        }

        @SuppressLint("StringFormatMatches")
        void onBind(final int position) {
            final FileModel fileModel = mData.get(position);
            labelTitle.setText(fileModel.getTitle());
            labelLastUpdateData.setText(fileModel.getLastUpdateData());

            int count = (int) fileModel.getChildFileCount();
            if (count == 0) {
                labelChildFileCount.setText(labelTitle.getContext().getString(R.string.file_count_zero));
            } else {
                labelChildFileCount.setText(labelTitle.getContext().getResources().getQuantityString(R.plurals.countOfFile, count, count));
            }

            labelFileSize.setText(labelTitle.getContext().getString(R.string.file_size, fileModel.getFileSize()));

            if (fileModel.isDirectory()) {
                labelImageView.setImageResource(R.drawable.ic_folder_list);
                labelFileSize.setVisibility(View.INVISIBLE);
                labelChildFileCount.setVisibility(View.VISIBLE);
            } else {
                labelImageView.setImageResource(R.drawable.ic_file_list);
                labelChildFileCount.setVisibility(View.INVISIBLE);
                labelFileSize.setVisibility(View.VISIBLE);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(fileModel);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mListener.onLongClick(fileModel);
                    return false;
                }
            });
        }
    }

    class FileManagerHolderGrid extends FileManagerHolder {
        private final ImageView labelImageView;
        private final TextView labelTitle;
        private final TextView labelLastUpdateData;

        FileManagerHolderGrid(@NonNull View itemView) {
            super(itemView);
            labelImageView = itemView.findViewById(R.id.label_image_view);
            labelTitle = itemView.findViewById(R.id.label_view_title);
            labelLastUpdateData = itemView.findViewById(R.id.label_last_update_data);
        }

        @SuppressLint("StringFormatMatches")
        void onBind(final int position) {
            final FileModel fileModel = mData.get(position);
            labelTitle.setText(fileModel.getTitle());
            labelLastUpdateData.setText(fileModel.getLastUpdateData());

            if (fileModel.isDirectory()) {
                labelImageView.setImageResource(R.drawable.ic_folder_grid);
            } else {
                labelImageView.setImageResource(R.drawable.ic_file_grid);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(fileModel);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mListener.onLongClick(fileModel);
                    return false;
                }
            });
        }
    }
}
