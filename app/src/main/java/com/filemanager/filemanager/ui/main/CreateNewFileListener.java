package com.filemanager.filemanager.ui.main;

public interface CreateNewFileListener {
    void onCreated(boolean created);
}
